<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\Product\ProductRepositoryInterface;

class ProductApiController extends Controller
{
    //
    protected $ProductRepository;

    public function __construct(ProductRepositoryInterface $ProductRepository)
    {
        $this->ProductRepository = $ProductRepository;
    }

    public function index(){

        $product = $this->ProductRepository->getAll();
        
        return $product;
    }

    public function show($id){
        $product = $this->ProductRepository->find($id);
        return $product;
    }


    public function store(Request $request){

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $extention = $file->getClientOriginalName();
            $file_name = time().'.'.$extention;
            $file->move('public/img',$file_name);
            $product = $this->avatar = $file_name;
        }
        $product = $this->ProductRepository->create($request->all());

        return $product;
    }


    public function update($id, Request $request){
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $extention = $file->getClientOriginalName();
            $file_name = time().'.'.$extention;
            $file->move('public/img',$file_name);
            $product = $this->avatar = $file_name;
        }
        $product = $this->ProductRepository->update($id, $request->all());
        return $product;
    }

    public function delete($id){
        $product = $this->ProductRepository->delete($id);
        return $product;
    }
}
