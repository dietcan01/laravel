<?php
namespace App\Repositories\product;

use App\Repositories\BaseRepository;

use App\Repositories\Product\ProductRepositoryInterface;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\productApi::class;
    }

    public function getProduct()
    {
       return $this->model->take(5)->get();
    }
}
