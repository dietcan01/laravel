<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productApi extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    protected $table = 'product_apis';

    protected $fillable = [
        'name',
        'avatar',
        'price',
        'description'
    ];

    public $timestamps = false;
}
