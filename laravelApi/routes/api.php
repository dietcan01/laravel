<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('index',[ProductApiController::class,'index'])->name('indexApi');

Route::post('store',[ProductApiController::class,'store'])->name('store');

Route::put('update/{id}',[ProductApiController::class,'update'])->name('update');

Route::delete('delete/{id}',[ProductApiController::class,'delete'])->name('delete');
